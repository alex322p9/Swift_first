func findIndexWithGenerics<T: Equatable>(of valueToFind: T, in array: [T]) -> Int? {
    
    for (index, value) in array.enumerated() {
        if value == valueToFind {
            return index
        }
    }
    return nil
}


let arrayOfString = ["кот", "пес", "лама", "попугай", "черепаха"]
if let foundIndexOfString = findIndexWithGenerics(of: "лама", in: arrayOfString) {
    print("Индекс ламы: \(foundIndexOfString)")
}

let arrayOfDouble = [4.1342, 11.11, 6.12456, 8867.994, 2.00004, 0.23]
if let foundIndexOfDouble = findIndexWithGenerics(of: 8867.994, in: arrayOfDouble) {
    print("Индекс для 8867.994: \(foundIndexOfDouble)")
}

let arrayOfInt = [4, 78, 87, 3, 44, 4]
if let foundIndexOfInt = findIndexWithGenerics(of: 4, in: arrayOfInt) {
    print("Индекс для 4: \(foundIndexOfInt)")
}
