import Foundation

func checkPrimeNumber(_ number: Int) -> Bool {
    if number <= 1 {
        return false
    }

    for i in 2..<number  {
        if number % i == 0 {
            return false
        }
    }

    return true
}

print(checkPrimeNumber(1))  // false
print(checkPrimeNumber(7))  // true
print(checkPrimeNumber(8))  // false
print(checkPrimeNumber(13)) // true
print(checkPrimeNumber(157)) // true
print(checkPrimeNumber(158)) // false
