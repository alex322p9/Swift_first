import Foundation


open class Shape {
    
    open func calculateArea() -> Double {
        fatalError("Not implemented")
    }
    
    open func calculatePerimeter() -> Double {
        fatalError("Not implemented")
    }
}


final class Rectangle: Shape {
    
    private let height: Double
    private let width: Double
    
    init(height: Double, width: Double) {
        self.width = width
        self.height = height
    }
    
    public override func calculateArea() -> Double {
        return width * height
    }
    
    public override func calculatePerimeter() -> Double {
        return 2 * (width + height)
    }
}


final class Circle: Shape {
    
    private let radius: Double
    
    init(radius: Double) {
        self.radius = radius
    }
    
    public override func calculateArea() -> Double {
        return Double.pi * radius * radius
    }
    
    public override func calculatePerimeter() -> Double {
        return 2 * Double.pi * radius
    }
}


final class Square: Shape {
    
    private let side: Double
    
    init(side: Double) {
        self.side = side
    }
    
    public override func calculateArea() -> Double {
        return side * side
    }
    
    public override func calculatePerimeter() -> Double {
        return 4 * side
    }
}

// Проверка

// Массив shapes с разными типами фигур
let shapes: [Shape] = [
    Rectangle(height: 1, width: 2),
    Circle(radius: 4),
    Square(side: 4)
]

// Счетчики сумм площадей и периметров массива фигур
var area: Double = 0
var perimeter: Double = 0

// Вычисление площади и периметра для каждой фигуры в массиве
for shape in shapes {
    area += shape.calculateArea()
    perimeter += shape.calculatePerimeter()
}


print("Сумма площадей: \(area)")
print("Сумма периметров: \(perimeter)")
