import Foundation

// Exercise 1
var milkmanPhrase = "Молоко - это полезно"

print(milkmanPhrase)


// Exercise 2
var milkPrice: Double = 3


// Exercise 3
milkPrice = 4.20


// Exercise 4
var milkBottleCount: Int? = 20
var profit: Double = 0.0

profit = milkPrice * Double(milkBottleCount!)
print(profit)


// Exercise 5
var employeesList: [String] = []
employeesList.append(contentsOf: ["Иван", "Петр", "Геннадий", "Андрей", "Марфа"])


// Exercise 6
var isEveryoneWorkHard : Bool = false
var workingHours: Int = 30

if workingHours >= 40 {
    isEveryoneWorkHard = true
}
else {
    isEveryoneWorkHard = false
}

print(isEveryoneWorkHard)
