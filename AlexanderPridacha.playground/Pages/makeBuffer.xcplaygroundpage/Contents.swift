import Foundation


//Задание 1

func makeBuffer() -> (String) -> Void {
    var buffer = ""

    return { value in
        if value.isEmpty {
            print(buffer)
        }
        else {
            buffer += value
        }
    }
}

var buffer = makeBuffer()

buffer("Замыкание")
buffer(" использовать")
buffer(" нужно!")
buffer("") // Замыкание использовать нужно!
