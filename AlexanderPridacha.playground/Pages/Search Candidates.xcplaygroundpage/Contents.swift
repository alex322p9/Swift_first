protocol CandidateFilter {
    
    func filter(_ candidates: [Candidate]) -> [Candidate]
}

struct Candidate {
    
    enum Grade {
        
        case junior
        case middle
        case senior
    }
    
    /// Грейд (уровень) кандидата
    let grade: Grade
    /// Требуемая зарплата
    let requiredSalary: Int
    /// Полное имя кандидата
    let fullName: String
}

struct GradeFilter: CandidateFilter {
    
    typealias T = Candidate.Grade
    
    let targetGrade: T
    
    func filter(_ candidates: [Candidate]) -> [Candidate] {
        return candidates.filter { $0.grade == targetGrade }
    }
}

struct SalaryFilter: CandidateFilter {
    
    typealias T = Int
    
    let maxSalary: T
    
    func filter(_ candidates: [Candidate]) -> [Candidate] {
        return candidates.filter { $0.requiredSalary <= maxSalary }
    }
}

struct NameFilter: CandidateFilter {
    
    typealias T = String
    
    let searchString: T
    
    func filter(_ candidates: [Candidate]) -> [Candidate] {
        return candidates.filter { $0.fullName.contains(searchString) }
    }
}

extension Candidate.Grade {
    
    var order: Int {
        switch self {
        case .junior:
            return 1
        case .middle:
            return 2
        case .senior:
            return 3
        }
    }
}

struct MinGradeFilter: CandidateFilter {
    
    typealias T = Candidate.Grade
    
    let minGrade: T
    
    func filter(_ candidates: [Candidate]) -> [Candidate] {
        return candidates.filter { $0.grade.order >= minGrade.order }
    }
}



// Проверка работы фильтров

let candidates = [
    Candidate(grade: .junior, requiredSalary: 580, fullName: "Alexey Solodilov"),
    Candidate(grade: .junior, requiredSalary: 4000, fullName: "Boris Borisov"),
    Candidate(grade: .middle, requiredSalary: 3700, fullName: "Mikhal Palych Terentyev"),
    Candidate(grade: .junior, requiredSalary: 1000, fullName: "Petya"),
    Candidate(grade: .senior, requiredSalary: 2200, fullName: "Mikle Scott"),
    Candidate(grade: .senior, requiredSalary: 3300, fullName: "Satoshi Nakamoto")
]

let gradeFilter = GradeFilter(targetGrade: .junior)
let juniorCandidates = gradeFilter.filter(candidates)
print(juniorCandidates)

let salaryFilter = SalaryFilter(maxSalary: 3700)
let filteredBySalary = salaryFilter.filter(candidates)
print(filteredBySalary)

let nameFilter = NameFilter(searchString: "Palych")
let filteredByName = nameFilter.filter(candidates)
print(filteredByName)

let minGradeFilter = MinGradeFilter(minGrade: .middle)
let filteredByMinGrade = minGradeFilter.filter(candidates)
print(filteredByMinGrade)
